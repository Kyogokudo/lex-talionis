# Build Script for the_lion_throne
# In Data/config.ini, turn cheat to 0
# In Data/config.ini, turn Screen Size to 3 and temp_ScreenSize to 3
# Make sure your old copy of the lion throne is backed up somewhere
# Remember to "source venv/Scripts/activate" before building!
rm -rf ../sword_at_sunset
mkdir ../sword_at_sunset
pyinstaller -y sword_at_sunset.spec
mv dist/sword_at_sunset ../sword_at_sunset/sword_at_sunset
cp ../lex-talionis-utilities/audio_dlls/* ../sword_at_sunset/sword_at_sunset
cp -r Audio ../sword_at_sunset/sword_at_sunset
cp -r Data ../sword_at_sunset/sword_at_sunset
cp -r Sprites ../sword_at_sunset/sword_at_sunset
#cp -r Saves ../sword_at_sunset/sword_at_sunset
mkdir ../sword_at_sunset/sword_at_sunset/Saves
cp double_click_to_play.bat ../sword_at_sunset
echo Done