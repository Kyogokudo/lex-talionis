if;self.name == 'upvill'
	area_replace_tile;17,0;destroyed_villagedata
	change_tile_sprite;17,0;destroyed_village;destroy
	set_tile_info;18,2
end
if;self.name == 'downvill'
	area_replace_tile;22,16;destroyed_villagedata
	change_tile_sprite;22,16;destroyed_village;destroy
	set_tile_info;23,18
end
if;self.name == 'downsnag'
	area_replace_tile;2,16;upsnagdata
	change_tile_sprite;2,16;upsnag;destroy
	set_tile_info;2,18
end
if;self.name == 'upsnag'
	area_replace_tile;8,2;rightsnagdata
	change_tile_sprite;8,2;rightsnag;destroy
	set_tile_info;8,2
end
if;self.name == 'rightsnag'
	area_replace_tile;31,9;upsnagdata
	change_tile_sprite;31,9;upsnag;destroy
	set_tile_info;31,11
end