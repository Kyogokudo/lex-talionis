# Prebase Script 7
add_to_market;Iron Sword
add_to_market;Rusted Sword
add_to_market;Iron Lance
add_to_market;Rusted Lance
add_to_market;Iron Axe
add_to_market;Rusted Axe
add_to_market;Iron Bow
add_to_market;Rusted Bow
add_to_market;Lightning
add_to_market;Flux
add_to_market;Heal
add_to_market;Vulnerary
#Bargain
add_to_market;Javelin
remove_from_market;Hand Axe
remove_from_market;Longbow

if;not gameStateObj.check_dead('Veles')
	unlock_lore;Veles
end

if;not gameStateObj.check_dead('Veles') and not gameStateObj.check_dead('Rokh')
	set_base_convo;Thievery
end
if;not gameStateObj.check_dead('Plinius')
	set_base_convo;Plinius and William
end

t;1
b;Forest
t;2
m;Ambient Forest
u;Beowulf;OffscreenLeft
u;Wiglaf;OffscreenLeft
move_sprite;Beowulf;Left
u;Eve;OffscreenRight
u;Ohla;OffscreenRight
qmove_sprite;Eve;Left
wait;100
qmove_sprite;Ohla;FarLeft
wait;100
r;Ohla
r;Eve
wait;100
mirror;Beowulf
s;Beowulf;!
wait;200
r;Beowulf
u;Beowulf;Right
move_sprite;Wiglaf;FarLeft
s;Wiglaf;Is there something wrong ?
mirror;Beowulf
s;Beowulf;No, it's nothing...
u;Perun;OffscreenRight
s;Perun;Commander !
wait;200
r;Beowulf
u;Beowulf;Left
move_sprite;Perun;Right
s;Perun;How did your meeting with Grendel go ? Shall we help him ?
s;Beowulf;His pride did not allow him to. By this time I believe Falham{w}{br} has already fallen. Maybe now the other rulers will{w}{br} finally realize that they need to unite.
s;Perun;Is that so ? Wait, where is Veles ?
if;not gameStateObj.check_dead('Veles')
	s;Beowulf;He left us. I suppose he couldn't bear to stand there{w}{br} doing nothing for the prisoners.
	s;Perun;This insufferable rat ! How dares he betray you like this ?{w}{br} He shall taste my lance !
	s;Beowulf;Leave him be. We have more important matters to attend to.
else
	s;Beowulf;He left us. I suppose he couldn't bear to stand there{w}{br} doing nothing for the prisoners.
	set_expression;Beowulf;CloseEyes
	s;Beowulf;He...died for that.
	set_expression;Wiglaf;CloseEyes
	set_expression;Perun;CloseEyes
	s;Perun;The fool ! I could never understand him but still...
	s;Beowulf;...
end
r;Beowulf
r;Perun
r;Wiglaf

t;1
b;TacticsRoom
t;2
m;Morale Fades
u;Lucan;FarLeft
u;William;Left
u;Maven;Right
s;Lucan;With the fort now ours, what should we do ? Surely the{w}{br} vasterians won't give up on it like that...
s;William;That is true, however the King has requested that we{w}{br} come back to the capital. We will leave most of the army{w}{br} here. I suppose he needs us for other tasks. By the way,{w}{br} Lady Maven, what of the situation in Durham ?
s;Maven;Rumors are that a few...diggers there took up arms because{w}{br} of the precarious situation they are in. Sir Calogrenant and Ywain is still{w}{br} trying to ease the tensions.
s;Lucan;Why...
set_expression;William;HalfCloseEyes
s;William;I pray that this doesn't end up like that rebellion...
s;Maven;Which one are you referring to ?
set_expression;William;NormalBlink
s;William;Moving on. We should focus on defending the fort for now.{w}{br} Our scouts should come back in a moment.
qr;Lucan
qr;William
qr;Maven