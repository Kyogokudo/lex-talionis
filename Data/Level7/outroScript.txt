t;1
b;OutsideFortSunset
t;2
u;Maven;Left
u;Lucan;OffscreenRight
move_sprite;Lucan;Right
s;Maven;Lucan ? What is going on ?
s;Lucan;We're under attack ! Sir William is holding the line alone...{w}{br}where is the other division ?
s;Maven;The gothian mercenaries ?
s;Lucan;No, it's another group and they're stronger...have you seen them ?
s;Maven;Nobody came close to the fort...are you sure that they{w}{br} are aiming for it and not something else ?
set_expression;Lucan;HalfCloseEyes
s;Lucan;Something else...
wait;400
bop;Lucan
set_expression;Lucan;NormalBlink
s;Lucan;Then Sir William is in danger !
mirror;Lucan
move_sprite;Lucan;OffscreenRight
r;Maven
r;Lucan
remove_unit;William
remove_unit;Eve
remove_unit;outro_cav_1
remove_unit;outro_cav_2
remove_unit;outro_cav_3
remove_unit;outro_cav_4
remove_unit;OhlaCH7
remove_unit;outro_rein_1
remove_unit;outro_rein_2
remove_unit;outro_rein_3
remove_unit;outro_rein_4
remove_unit;outro_rein_5
remove_unit;outro_rein_6
remove_enemies
add_unit;Lucan;0,12
remove_background
u;Lucan;OffscreenLeft
move_sprite;Lucan;Left
s;Lucan;Sir William !
move_unit;Lucan;3,12
start_move
m;The Streets of Whiterun
wait;400
s;Lucan;Sir...William...?
set_expression;Lucan;HalfCloseEyes
wait;300
s;Lucan;His head...no, it can't be...
if;not gameStateObj.check_dead('Colin')
	u;Colin;FarLeft;HalfCloseEyes
	s;Colin;Lord William ! Curse !! We were so foolish...
	r;Colin
end
if;not gameStateObj.check_dead('Rokh')
	u;Rokh;FarLeft;HalfCloseEyes
	s;Rokh;Talk about becoming a true knight...I can't even protect my liege...
	r;Rokh
end
if;not gameStateObj.check_dead('Silverius')
	u;Silverius;FarLeft;HalfCloseEyes
	s;Silverius;This sight again...the least I can do is pray for his soul.
	r;Silverius
end
bop;Lucan
s;Lucan;What am I to do now...?
r;Lucan 

t;1
b;OutsideFortSunset
t;2
u;Soldier1;FarLeft
u;Lucan;OffscreenRight
move_sprite;Lucan;Right
s;Lucan;Soldier, where is Lady Maven ?
s;Soldier1;Sir ! She left ! She said she wanted to warn the{w}{br} King of what happened as soon as possible.
set_expression;Lucan;CloseEyes
s;Lucan;I see...then I am the highest ranked person here. I have to lead...
wait;500
set_expression;Lucan;NormalBlink
if;not gameStateObj.check_dead('Colin')
	u;Colin;Left
	s;Colin;We will be fine. I trust in you, Sir Lucan !
	s;Lucan;Colin...thank you.
end
s;Lucan;Strengthen the defenses of the fort ! Guard duties will be done{w}{br} in small groups and kept close to the fort. Those assassins{w}{br} might still lurk somewhere. We will return to{w}{br} Loegres in two days !