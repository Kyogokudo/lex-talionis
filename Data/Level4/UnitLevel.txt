# UnitLevel.txt is used to define what units will be part of this level and where they will spawn
# 
# Each unit belongs on its own line
# Syntax:
# New Units:
# team; 0; event_id; class; level; items; position; ai; faction; status (optional)
# - OR -
# Named units:
# team; 1; event_id; unit_id; position; ai
# - OR -
# Created Units:
# team; 2; event_id; class; items; position; ai; faction; status (optional)
# 
# event_id gives the unit a unique id that scripts can use. The unit will not start on the battlefield unless event_id == 0.
# unit_id - unit to load from the units.xml file
# position should be formatted like #,#
# ai refers to what kind of AI the unit should possess.
#
# --------------------------------------------
load_player_characters
faction;Mercenaries;Mercenaries;Knight;A mercenary squad tasked with the protection of Falhams supplies.
faction;Falham;Falham;Knight;The vast army of Vasters Fort Falham.
# Player Characters
player;1;0;Lucan;0,10;None
# Other Characters
other;0;0;Carriage;1,11;None
other;0;0;Barrel1;1,2;None
other;0;0;Barrel2;3,16;None
other;0;0;Barrel3;16,0;None
other;0;0;Barrel4;13,17;None
other;0;0;Barrel5;28,17;None
other;0;0;Barrel6;28,9;None
# Enemies
# Bosses
enemy;0;0;Uttrakad;28,10;SoftGuardIgnoreOther
# Generics
enemy;0;0;Archer;5;Longbow;11,1;HardGuard;Mercenaries
enemy;0;0;Archer;5;Longbow;21,1;HardGuard;Mercenaries
enemy;0;0;Mercenary;6;Armorslayer;15,0;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Soldier;6;Horseslayer;23,15;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Myrmidon;4;Iron Sword;4,17;PursueIgnoreOther;Mercenaries
enemy;0;0;Fighter;4;Iron Axe;0,2;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Nomad;3;Iron Bow;6,0;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Mercenary;4;Iron Sword;11,10;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Mage;5;Fire;16,7;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Archer;5;Short Bow;13,16;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Cavalier;3;Iron Sword,Iron Lance;29,13;PursueIgnoreOther;Mercenaries
enemy;0;0;Cavalier;3;Iron Sword,Iron Lance;29,11;PursueIgnoreOther;Mercenaries
enemy;0;0;Fighter;4;Iron Axe;21,7;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Myrmidon;2;Iron Sword;11,3;HardGuard;Mercenaries
enemy;0;0;Myrmidon;2;Iron Sword;21,3;HardGuard;Mercenaries
enemy;0;0;Myrmidon;7;dKilling Edge;27,6;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Soldier;6;Steel Lance;18,13;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Nomad;3;Iron Bow;27,0;PursueIgnoreOther;Mercenaries
enemy;0;0;Brigand;5;Iron Axe;15,11;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Fighter;4;Iron Axe;14,17;SoftGuardIgnoreOther;Mercenaries
enemy;0;0;Brigand;3;Iron Axe;0,17;PursueIgnoreOther;Mercenaries
enemy;0;0;Archer;3;Iron Bow;29,17;SoftGuardIgnoreOther;Mercenaries
# === Reinforcements ===
# Player Characters
# Enemies
# Generics
enemy;0;topRightCav_2;Cavalier;4;Iron Sword;24,0;PursueIgnoreOther;Mercenaries
enemy;0;topRightCav_3;Cavalier;4;Iron Lance;23,0;PursueIgnoreOther;Mercenaries
enemy;0;leftCav_1;Cavalier;2;Iron Lance;0,10;PursueIgnoreOther;Mercenaries
enemy;0;leftCav_2;Cavalier;2;Iron Sword;0,11;PursueIgnoreOther;Mercenaries
enemy;0;lateRein_5;Soldier;14;Steel Lance;1,10;PursueIgnoreOther;Falham
enemy;0;lateRein_1;Soldier;14;Steel Lance;1,11;PursueIgnoreOther;Falham
enemy;0;lateRein_2;Nomad;9;Iron Bow;0,9;PursueIgnoreOther;Falham
enemy;0;lateRein_3;Nomad;9;Iron Bow;0,12;PursueIgnoreOther;Falham
enemy;0;lateRein_4;Myrmidon;13;Longsword;8,0;PursueIgnoreOther;Falham
enemy;0;lateRein_6;Myrmidon;13;Armorslayer;26,0;PursueIgnoreOther;Falham
enemy;0;topLeft_1;Soldier;5;Iron Lance;8,0;PursueIgnoreOther;Mercenaries
enemy;0;topLeft_2;Fighter;5;Iron Axe;4,0;PursueIgnoreOther;Mercenaries
enemy;0;rightCorner_1;Archer;4;Iron Bow;29,4;PursueIgnoreOther;Mercenaries
enemy;0;rightCorner_2;Mercenary;5;Iron Sword;29,6;PursueIgnoreOther;Mercenaries
# === Triggers ===
trigger;introSpawn;29,13;30,13;29,13
trigger;introSpawn;23,15;28,12;23,15
trigger;introSpawn;11,1;16,1;11,1
trigger;introSpawn;6,0;6,-1;6,0
trigger;introSpawn;27,0;26,-1;27,0
trigger;introSpawn;15,11;17,9;15,11
trigger;introSpawn;18,13;15,17;18,13
trigger;introSpawn;29,11;30,11;29,11
trigger;introSpawn;11,3;15,7;11,3
trigger;introSpawn;21,7;21,5;21,7
trigger;introSpawn;0,2;1,3;0,2
trigger;introSpawn;4,17;4,16;4,17
trigger;introSpawn;11,10;16,9;11,10
trigger;introSpawn;16,7;16,8;16,7
trigger;introSpawn;21,3;22,4;21,3
trigger;introSpawn;21,1;17,0;21,1
trigger;allySpawn;Carriage;-1,11;1,11
trigger;allySpawn;Lucan;-1,10;0,10
