if;self.unit.name == 'Heimir'
    if;self.unit2.name == 'Uttrakad'
        if;'UttrakadHeimir' not in gameStateObj.level_constants
            u;Uttrakad;Left
			u;Heimir;Right
            s;Uttrakad;That's a familiar face. You're from that other{w}{br} mercenary company that was employed ?
			s;Heimir;I was. Now I work for the other side. Want to defect too ?
			s;Uttrakad;Pfftahaha ! You already looked odd when I first saw you{w}{br} but now that's hilarious ! Why would I ever betray my employer ?
			s;Heimir;So you'd continue being used by that Grendel ?{w}{br} Thought you vasterians had more honor than that.
			s;Uttrakad;Ah ! Who he is doesn't matter a single bit for me. As long {w}{br}as he keeps paying us that's fine.{w}{br} But enough talk, let's have a good fight !
			r;Heimir
            r;Uttrakad
            set_level_constant;UttrakadHeimir
        end
    end
elif;self.unit.team == 'player'
    if;self.unit2.name == 'Uttrakad'
        if;'UttrakadDefault' not in gameStateObj.level_constants
            u;Uttrakad;Left
            s;Uttrakad;You don't look half bad...let's see if you can keep me occupied for a bit.
            r;Uttrakad
            set_level_constant;UttrakadDefault
        end
    end
end