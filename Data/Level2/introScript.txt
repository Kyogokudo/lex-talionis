#Info dump
t;1
b;BaseBackground
t;2
m;March Forward
u;Empty;FarRight
s;Empty;Gothia, situated at the east of Albion, is made up of 3 petty kingdoms.{w}{br} Oster to the north, Varm to the east and Vaster to the west.{w}{br} These provinces were once a part of the unified kingdom of Gothia,{w}{br} under the ruling of King Knut.{w}{br} After his death, his sons all claimed to be the legitimate successor.{w}{br} Thus they each found and named their dominion. Gothian soils are fertile,{w}{br} and there are countless forests. This stands in opposition{w}{br} to Albion's barren lands and mountains.{w}{br} The entrance to Vaster and to the country as a whole is fort Falham.{w};auto;narration
qr;Empty

#plains scene
# Fade to plains
t;1
b;Plains
t;2
u;Lucan;Left
u;William;Right
m;Ambient Forest
s;William;We are getting closer to Falham.{w}{br} I believe we should reach it in 2 days at this pace.
s;Lucan;Sir William, there are people coming to us.{w}{br} Cavaliers...are these Vasters ?
s;William;Hmm, looks like it. I didn't expect to meet them so soon though.
r;Lucan
r;William
remove_background

m;Aesthetics of Deprivation
trigger;Cav1
move_cursor;Strunt
wait;500
u;Strunt;Right
s;Strunt;Go ahead and try to intercept the albionese army before they strike they said.{w}{br} Pfeh, like there would be anyone here !{w}{br} These plains are as barren as my wallet after a few drinks !
u;Soldier1;OffscreenLeft
move_sprite;Soldier1;Left
s;Soldier1; Captain, we found the albine army ! They're coming here !
s;Strunt; What, really ?! Ok here's my chance.{w}{br} Call everyone ! We need that balista.{w}{br} Also tell that old knight to stand at the bridge.{w}{br} They won't get past him so I'll be fine here.{w}{br} Don't forget the reinforcments too !
r;Soldier1
r;Strunt

move_cursor;Lucan
wait;200
u;Lucan;Left
u;William;Right
s;William;Well that didn't take long. Once again I will let you handle this.{w}{br} Don't hold back this time.
r;William

if;not gameStateObj.check_dead('Rokh')
	u;Rokh;Right
	s;Rokh;Ah we are finally getting to the interesting things ! Let's go !
	r;Rokh
end

if;not gameStateObj.check_dead('Barlow')
	u;Barlow;Right
	s;Barlow;I just joined and we're already into a skirmish.{w}{br} Eh, not like I have any objection to this.
end