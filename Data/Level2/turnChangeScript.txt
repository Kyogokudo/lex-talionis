#Strunt mentions the forts rein on t3
if;gameStateObj.turncount == 3
	u;Strunt;Right
	s;Strunt; Bah, what are those idiots in the forts doing ?{w}{br} Get out of here and fight already !
	r;Strunt
end
#Cavaliers appear on the forts on turn 4
if;gameStateObj.turncount == 4
    #set cursor sur les cavaliers
    add_unit;fortrein1_1;10,11
    set_cursor;10,11
    disp_cursor;1
    wait;500
    add_unit;fortrein1_2;3,8
    set_cursor;3,8
    wait;500
    disp_cursor;0
end
#Strunt mentions the peg rein on t5
if;gameStateObj.turncount == 5
    u;Strunt;Right
	s;Strunt; Those bloody pegasus knights too !{w}{br} They asked for so much money yet they can't even attack from behind ?!
	r;Strunt
end
#Peg knights appear on the top right corner of the screen on turn 6
if;gameStateObj.turncount == 6
    add_unit;peg1_1;15,0
    #set cursor sur les pégases
    set_cursor;15,0
    disp_cursor;1
    wait;500
    add_unit;peg1_2;16,1
    set_cursor;16,1
    wait;500
    disp_cursor;0
end
#Peg knights appear on the top right corner of the screen on turn 10
if;gameStateObj.turncount == 10
    add_unit;peg2_1;15,0
    #set cursor sur les pégases
    set_cursor;15,0
    disp_cursor;1
    wait;500
    add_unit;peg2_2;16,1
    set_cursor;16,1
    wait;500
    disp_cursor;0
end
#Strunt mentions the cav rein on t12
if;gameStateObj.turncount == 12
    u;Strunt;Right
	s;Strunt;Curse those lazy cavaliers too !
	r;Strunt
end
#cav appear at the start of the map on turn 13
if;gameStateObj.turncount == 13
    add_unit;cavrein1_1;2,0
    set_cursor;2,0
    disp_cursor;1
    wait;500
    add_unit;cavrein1_2;5,0
    set_cursor;5,0
    wait;500
	add_unit;cavrein1_3;3,0
    set_cursor;3,0
    disp_cursor;1
    wait;500
	add_unit;cavrein1_4;4,0
    set_cursor;4,0
    disp_cursor;1
    wait;500
    disp_cursor;0
end
if;'VisitedVillage' in gameStateObj.level_constants
	convert;Eir;player
end