name;Ch2: Unexpected Skirmish
prep_flag;0
pick_flag;1
base_flag;0
market_flag;0
transition_flag;1
display_name;Defeat Boss
win_condition;Defeat Strunt
loss_condition;Lucan dies
player_phase_music;Proud Fight
enemy_phase_music;Proud Fight
other_phase_music;Proud Fight
weather;Light
