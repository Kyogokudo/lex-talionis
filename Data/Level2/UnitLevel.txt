# UnitLevel.txt is used to define what units will be part of this level and where they will spawn
# 
# Each unit belongs on its own line
# Syntax:
# New Units:
# team; 0; event_id; class; level; items; position; ai; faction; status (optional)
# - OR -
# Named units:
# team; 1; event_id; unit_id; position; ai
# - OR -
# Created Units:
# team; 2; event_id; class; items; position; ai; faction; status (optional)
# 
# event_id gives the unit a unique id that scripts can use. The unit will not start on the battlefield unless event_id == 0.
# unit_id - unit to load from the units.xml file
# position should be formatted like #,#
# ai refers to what kind of AI the unit should possess.
#
# --------------------------------------------
faction;Vaster;Vaster;Knight;A detachment of fort Falham's army.
# Player Characters
player;1;0;Lucan;3,1;None
player;1;0;Colin;2,2;None
player;1;0;Rokh;4,2;None
player;1;0;Barlow;5,1;None
# Other Characters
# Enemies
# Bosses
enemy;0;0;Strunt;14,22;HardGuard
# Generics
enemy;0;0;LanceKnight;13;dJavelin;8,17;HardGuard;Vaster
enemy;0;0;Myrmidon;4;Iron Sword;1,6;SoftGuardIgnoreOther;Vaster
enemy;0;0;Ballista;1;Ballista;1,20;HardGuard;Vaster
enemy;0;0;Fighter;4;Iron Axe;3,11;SoftGuardIgnoreOther;Vaster
enemy;0;0;Mercenary;3;Iron Sword;10,7;PursueIgnoreOther;Vaster
enemy;0;0;Soldier;5;Iron Lance;12,10;SoftGuardIgnoreOther;Vaster
enemy;0;0;Cavalier;1;Iron Lance,Iron Sword;8,22;PursueIgnoreOther;Vaster
enemy;0;0;Cavalier;1;Iron Lance,Iron Sword;10,22;PursueIgnoreOther;Vaster
enemy;0;0;Fighter;3;Hand Axe;8,14;SoftGuardIgnoreOther;Vaster
enemy;0;0;Soldier;4;Iron Lance;4,19;SoftGuardIgnoreOther;Vaster
enemy;0;0;Brigand;3;Rusted Axe;12,9;PursueVilIgnOther;Vaster
# === Reinforcements ===
# Player Characters
player;0;eirRein_1;Eir;9,3;None
# Enemies
# Generics
enemy;0;fortrein1_1;Cavalier;5;Iron Lance;None;PursueIgnoreOther;Vaster
enemy;0;fortrein1_2;Cavalier;5;Iron Sword;None;PursueIgnoreOther;Vaster
enemy;0;peg1_1;PegasusknightF;3;Iron Lance;15,0;PursueIgnoreOther;Vaster
enemy;0;peg1_2;PegasusknightF;3;Iron Lance;16,1;PursueIgnoreOther;Vaster
enemy;0;peg2_1;PegasusknightF;3;Iron Lance;15,0;PursueIgnoreOther;Vaster
enemy;0;peg2_2;PegasusknightF;3;Iron Lance;16,1;PursueIgnoreOther;Vaster
enemy;0;cavrein1_1;Cavalier;6;Iron Lance;2,0;PursueIgnoreOther;Vaster
enemy;0;cavrein1_2;Cavalier;6;Iron Lance;5,0;PursueIgnoreOther;Vaster
enemy;0;cavrein1_3;Cavalier;6;Iron Sword;3,0;PursueIgnoreOther;Vaster
enemy;0;cavrein1_4;Cavalier;6;Iron Sword;4,0;PursueIgnoreOther;Vaster
# === Triggers ===
trigger;EirSpawn;Eir;11,-1;11,3
