name;Ch6: Assault on Falham
current_party;0
prep_flag;1
prep_music;Backsliding
pick_flag;1
base_flag;BaseEntrance
base_music;The Task at Hand
market_flag;1
transition_flag;1
player_phase_music;Impregnable Defence
enemy_phase_music;Impregnable Defence
other_phase_music;Impregnable Defence
display_name;Seize
win_condition;Seize the throne
loss_condition;Lucan or William dies
weather;Light
