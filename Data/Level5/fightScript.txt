if;self.unit.name == 'Plinius'
    if;self.unit2.name == 'Skryt'
        if;'SkrytPlinius' not in gameStateObj.level_constants
            u;Skryt;Left
			u;Plinius;Right
            s;Plinius;Ooh this is quite the peculiar tome {w}{br}you have here, mind if I take a look ?
			s;Skryt;A practitioner of that foul magic like you ? Ah !{w}{br} You wouldn't be able to understand the beauty of this spell !
			s;Plinius;What a waste...I suppose it will have to{w}{br} wait after I am done showing you what{w}{br} that "foul magic" can do.
            r;Skryt
			r;Plinius
            set_level_constant;SkrytPlinius
        end
    end
elif;self.unit.team == 'player'
    if;self.unit2.name == 'Skryt'
        if;'SkrytDefault' not in gameStateObj.level_constants
            u;Skryt;Left
            s;Skryt;Bow down to me, for I {w}{br}shall be the new commander of Falham !
            r;Skryt
            set_level_constant;SkrytDefault
        end
    end
end