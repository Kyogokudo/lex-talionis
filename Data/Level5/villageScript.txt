# Village Script
if;self.name == 'vill'
	if;self.unit.name == 'Plinius'
		# Transition in
		t;1
		b;TownDark
		t;2
		u;Man6;Right;u;{unit};Left
		s;Man6;Aah, you again ! Why did you come back ?
		s;{unit};Ah good sir, you needn't worry about the village anymore.{w}{br} I have allied up with the albionese knights and{w}{br} repelled the vasterians !
		s;Man6;Is that so...? I must offer you all my gratitude then...
		s;{unit};And with that out of the way, could you {w}{br}give me the book you promised ?
		set_expression;Man6;HalfCloseEyes
		s;Man6;*sigh* I cannot refuse, can I ? Here...
		s;{unit};Thank you, good sir !
		mirror;{unit}
		move_sprite;{unit};OffscreenLeft
		wait;200
		set_expression;Man6;CloseEyes
		s;Man6;*sigh* Why am I so unlucky...
		# Transition out
		t;1
		qr;Man6;{unit}
		change_tile_sprite;15,5;closed
		set_tile_info;15,5
		replace_tile;15,5;1
		remove_background
		t;2
		# Close the village
		set_origin
		set_tile_info;o0,0
		give_item;{unit};Secret Book
	else
		# Transition in
		t;1
		b;TownDark
		t;2
		u;Man6;Right;u;{unit};Left
		s;Man6;You aga...Ooh, knights, I am glad to see you !{w}{br} Have you secured our small village ? Good, then{w}{br} please take this precious book before someone else come back{w}{br} and beg for it...
		# Transition out
		t;1
		qr;Man6;{unit}
		change_tile_sprite;15,5;closed
		set_tile_info;15,5
		replace_tile;15,5;1
		remove_background
		t;2
		# Close the village
		set_origin
		set_tile_info;o0,0
		give_item;{unit};Secret Book
	end
end