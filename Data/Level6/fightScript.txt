if;self.unit.name == 'Veles'
    if;self.unit2.id == 'Beowulf_CH6'
        if;'BeowulfVeles' not in gameStateObj.level_constants
            u;Beowulf_CH6;Left
			u;Veles;Right
			s;Beowulf_CH6;So the Tempest shows his true colors...{w}{br}Are you planning on never coming back to Varm ?
			s;Veles;Not like I have any ties to Varm or even Gothia to begin with.{w}{br} Your group isn't much better than Grendel's minions.
			s;Beowulf_CH6;Is that so ? Well then, let's see if you can best me !
            r;Beowulf_CH6
			r;Veles
            set_level_constant;BeowulfVeles
        end
	end
	if;self.unit2.id == 'Wiglaf_CH6'
		if;'WiglafVeles' not in gameStateObj.level_constants
			u;Wiglaf_CH6;Left
			u;Veles;Right
			s;Wiglaf_CH6;Veles..Why would you join the other side ?
			s;Veles;I just go wherever I want. That's how I earned my title after all.{w}{br} Besides you already know why.
			s;Wiglaf_CH6;You could just never get along with Perun. It only makes sense{w}{br} that you wouldn't understand the decision of the general...
			s;Veles;Tch, you didn't need to bring this guy's name up.{w}{br} But what are we doing talking like that ? Let's fight !
			r;Wiglaf_CH6
			r;Veles
			set_level_constant;WiglafVeles
		end
	end
elif;self.unit.name == 'Heimir'
    if;self.unit2.name == 'Grendel'
        if;'GrendelHeimir' not in gameStateObj.level_constants
            u;Grendel;Left
			u;Heimir;Right
			s;Grendel;You...you're from that ragtag band of mercenaries. How dare you point{w}{br} your sword at me after how much I paid you ?
			s;Heimir;You worked us to death, proceeded to take women from Oster hostage...{w}{br}and now you're doing the same with our commander.{w}{br} It's about time you pay for all this !
			s;Grendel;Ah, her...she is quite the lady for someone so young and hailing from Varm.{w}{br} Don't you worry, I will take great care of her.
			s;Heimir;...I don't even have words for this. I will just let my blade speak for myself.
			s;Grendel;If it can cut through my armor, that is !
            r;Grendel
            set_level_constant;GrendelHeimir
        end
    end
	if;self.unit2.id == 'Beowulf_CH6'
        if;'BeowulfDefault' not in gameStateObj.level_constants
            u;Beowulf_CH6;Left
			s;Beowulf_CH6;Why would you seek me out ? Hmph whatever, I won't back down from a challenge !
            r;Beowulf_CH6
            set_level_constant;BeowulfDefault
        end
    end
    if;self.unit2.id == 'Wiglaf_CH6'
        if;'WiglafDefault' not in gameStateObj.level_constants
            u;Wiglaf_CH6;Left
			s;Wiglaf_CH6;We had no intention on going against you here but you leave me no choice !
            r;Wiglaf_CH6
            set_level_constant;WiglafDefault
        end
    end
elif;self.unit.team == 'player'	
    if;self.unit2.id == 'Beowulf_CH6'
        if;'BeowulfDefault' not in gameStateObj.level_constants
            u;Beowulf_CH6;Left
			s;Beowulf_CH6;Why would you seek me out ? Hmph whatever, I won't back down from a challenge !
            r;Beowulf_CH6
            set_level_constant;BeowulfDefault
        end
    end
    if;self.unit2.id == 'Wiglaf_CH6'
        if;'WiglafDefault' not in gameStateObj.level_constants
            u;Wiglaf_CH6;Left
			s;Wiglaf_CH6;We had no intention on going against you here but you leave me no choice !
            r;Wiglaf_CH6
            set_level_constant;WiglafDefault
        end
    end
    if;self.unit2.name == 'Grendel'
        if;'GrendelDefault' not in gameStateObj.level_constants
            u;Grendel;Left
			s;Grendel;I am Grendel, commander of Falham and soon to be king of Gothia !{w}{br} Kneel before me, albionese worms !
            r;Grendel
            set_level_constant;GrendelDefault
        end
    end
end