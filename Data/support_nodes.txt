# Keeps track of every unit which has an affinity
# [name of unit];[affinity]
# Player Units
Ophie;Fire
Prim;Water
Joel;Water
Nia;Wind
Drake;Earth
Althea;Light
Renae;Earth
Theo;Dark
Sam;Dark
Eliza;Light
Coyote;Wind
Kayla;Fire

Lucan;Light
Colin;Water
Rokh;Wind
Barlow;Earth
Eir;Light
Heimir;Dark
Atalante;Earth
Plinius;Water
William;Lightning
Veles;Wind
Silverius;Ice
Vidga;Wind
Culex;Dark
Maven;Water
Ohla;Ice
Meliant;Water
Galaad;Light
#
# Named Enemy units (mostly Bosses)
Sidney;Earth
Brigham;Light
Kiley;Fire
Midas;Water
Harold;Earth
Jack;Wind
Vincent;Wind
Razia;Fire
Marduk;Dark
Vagnius;Fire
Jakin;Wind
Boaz;Earth
Ulver;Fire
Beowulf_CH6;Fire
Wiglaf_CH6;Wind
Ghreb;Earth
Strunt;Fire
Ratta;Wind
MavenGreen;Water
Uttrakad;Lightning
Skryt;Fire
Grendel;Earth
Vidga_CH6;Wind
OhlaCH3;Ice
OhlaCH7;Ice
Langsam;Earth
Atket;Dark
Bessas;Fire
#
# Other units
Yohn;Light