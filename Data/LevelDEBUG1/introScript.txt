#dialogues des rebelles
trigger;Watchman
wait;500
m;Aesthetics of Deprivation
u;Man5;Left
u;Eldon;Right
move_sprite;Man5;Left
s;Man5;They're heading this way mister Eldon.{w}{br} What should we do ?
s;Eldon;Ahaha great ! {w}{br}It is time to finally let them know what we think.{w}{br} To me proud citizens of this country !{w}{br} We have to stop their tyrannic practices !
r;Man5
r;Eldon
trigger;GroupSep
set_cursor;17,9
wait;500
u;Man4;Left
u;Teen1;Right
s;Man4;Hey you. What are you doing ? Move on already.
s;Teen1;B-but I'm afraid I don't want to do this...
s;Man4;We gotta stand together so these snobs can understand what we face everyday.{w}{br} Now go !
r;Man4
r;Teen1
trigger;Journeyman

#barlow
set_cursor;Barlow
wait;500
u;Barlow;Left
s;Barlow;That revolt was already a pretty bad idea but now they force teenagers into this ?{w}{br} This is getting ridiculous.{w}{br} On the other side there is clearly something wrong with the King's methods.{w}{br} I guess I'll just wait to see how those knights handle this.
r;Barlow

#arrivée du groupe de lucan
trigger;PartySpawn
set_cursor;Lucan
u;Lucan;Left
s;Lucan;Those villagers...they are armed.{w}{br} What is going on here ?
u;William;Right
s;William;My guess is that they were waiting to ambush us.{w}{br} But perhaps their leader wishes to speak to us ?
r;William
u;William;FarLeft
u;Eldon;Right
s;Eldon;So you came...{w}{br}The rumors were true then, that foolish King ordered you to invade Gothia.
s;Lucan;How did you know ? What is the meaning of this ?
s;Eldon;Pfeh, not like a new recruit like you could understand.{w}{br} See while you enjoy conquering some meaningless territories we are out here{w}{br} working those bare lands to death so we can fund your efforts.{w}{br} Now we're taking this matter into our hand,{w}{br} and this starts with suppressing you all.
s;William;I don't think anyone could understand this twisted logic of yours.{w}{br} Facing us would only mean more casualties{w}{br} for you and less people to help you work the lands.{w}{br} Besides I doubt you can even grasp a hold{w}{br} of what the King is trying to accomplish.{w}{br} Anyways this is your chance to prove your worth again Lucan.{w}{br} Cutting the head is often the best way to stop those situations.{w}{br} Some people might even be fighting against their will.
r;Eldon
r;William
r;Lucan
end_skip