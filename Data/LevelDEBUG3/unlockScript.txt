set_origin
if;self.name == 'gate'
	area_replace_tile;11,15;floordata
	change_tile_sprite;11,15;floor
	set_tile_info;9,2
end
if;self.name == 'rubychest'
	change_tile_sprite;11,8;chestopen
	set_tile_info;11,8
	give_item;{unit};Ruby
end