# Talk Script Ch3
# Lucan et Atalante
if;self.unit.name == 'Lucan' and self.unit2.name == 'Atalante'
	m;Stalwarts Unite
	u;Lucan;Right
	u;Atalante;Left
	s;Lucan;You don't seem to be vasterian, you should leave at once.{w}{br} This is a war zone and I can't guarantee your safety.
	s;Atalante;I can do that on my own, don't be deceived.{w}{br} I'm a pretty fine huntress after all.
	s;Lucan;Even if you are skilled at dealing with animals it won't make{w}{br} the cut here. I don't want to see any {w}{br}civilians get harmed either.
	s;Atalante;Tell me, what is it that you're trying to do ?{w}{br} Those lands are already barren and this fort in a{w}{br} poor state, yet you would continue ruining all of that ?
	s;Lucan;I can only say that I didn't want to fight here.{w}{br} We know this causes even more grief to people living nearby but{w}{br} we have to retaliate and keep moving to Vaster.
	s;Atalante;I understand what you're trying to do...
	set_expression;Atalante;Full_Blink
	wait;200
	set_expression;Atalante;Normal
	s;Atalante;Let me help you for now.
	s;Lucan;What !? But I just told you to not get involved !
	s;Atalante;You need more help to drive out them don't you ?{w}{br} We'll get that place the peace it deserves quickly.{w}{br} I want to see the lands of Gothia and improve my skills too.
	s;Lucan;...I can't refuse, can I ? Still I want you to be careful.
	s;Atalante;Don't worry, if something goes wrong I'll run{w}{br} away before you can even order me to !
	r;Lucan
	r;Atalante
	convert;Atalante;player
	remove_talk;Lucan;Atalante
end