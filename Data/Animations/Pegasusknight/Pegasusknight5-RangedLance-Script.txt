pose;Dodge
f;1;Lance_000
f;3;Lance_038
f;27;Lance_039
f;4;Lance_038

pose;Attack
f;6;Lance_000
f;6;Lance_034
f;10;Lance_035
sound;Weapon Push
f;1;Lance_035
f;3;Lance_036
spell;Javelin
f;1;Lance_037
start_loop
f;4;Lance_037
end_loop
f;4;Lance_037
f;4;Lance_033

pose;Critical
f;6;Lance_000
f;3;Lance_028
f;6;Lance_034
sound;Heavy Spear Spin
f;1;Lance_034
f;1;Lance_029
f;1;Lance_030
f;2;Lance_031
f;2;Lance_032
sound;Heavy Spear Spin
f;1;Lance_032
f;1;Lance_029
f;1;Lance_030
f;2;Lance_031
f;2;Lance_032
f;8;Lance_034
f;12;Lance_035
sound;Weapon Push
f;1;Lance_035
f;2;Lance_036
spell;Javelin
f;1;Lance_037
start_loop
f;4;Lance_037
end_loop
f;4;Lance_037
f;4;Lance_033

pose;Stand
f;4;Lance_000

