# Village Script
if;self.name == 'line1row1'
	# Transition in
	t;1
	b;House
	t;2
	u;Man5;Left
	s;Man5;Aaah, the royal knights are coming...{w}{br} Does that mean they're here for m...
	u;{unit};Right
	s;Man5;Oh...uh...welcome.{w}{br} Brigands are pillaging the villages ?{w}{br} O-ok, thanks for warning me...{w}{br} Y-you can have this I guess.
	# Transition out
	t;1
	qr;Man5;{unit}
	change_tile_sprite;1,5;closed
	set_tile_info;1,5
	replace_tile;1,5;1
	remove_background
	t;2
	# Close the village
	set_origin
	set_tile_info;o0,0
	gold;50
end
if;self.name == 'line1row2'
	# Transition in
	t;1
	b;House
	t;2
	u;Man2;Left;u;{unit};Right
	s;Man2;Bandits are raiding the village ? Thanks for warming us.{w}{br} I heard it was quite frequent lately but to think they would come here...{w}{br}Oh you can have this.
	# Transition out
	t;1
	qr;Man2;{unit}
	change_tile_sprite;4,5;closed
	set_tile_info;4,5
	replace_tile;4,5;1
	remove_background
	t;2
	# Close the village
	set_origin
	set_tile_info;o0,0
	give_item;{unit};Vulnerary
end
if;self.name == 'line2row1'
	# Transition in
	t;1
	b;House
	t;2
	u;OldMan1;Left;u;{unit};Right
	s;OldMan1;Youngsters these days, always waving their iron or steel shticks like{w}{br} they're some sort of vet.{w}{br} Lemme tell ya, back in our days we only used rusted weapons for training. {w}{br}And it was much better ! If ya can understand that,{w}{br} I'll let ya have this lance over there.{w}{br} It's not like I can use it anymore with that back of mine.
	# Transition out
	t;1
	qr;OldMan1;{unit}
	change_tile_sprite;18,8;closed
	set_tile_info;18,8
	replace_tile;18,8;1
	remove_background
	t;2
	# Close the village
	set_origin
	set_tile_info;o0,0
	give_item;{unit};Rusted Lance
end
if;self.name == 'line3row1'
	# Transition in
	t;1
	b;House
	t;2
	u;Woman1;Left;u;{unit};Right
	s;Woman1;My, bandits ?!{w}{br} Say this man over here, wouldn't he be Ghreb ?{w}{br} See I met him a few years ago. He was just a simple shopkeeper from a nearby village.{w}{br} To think he would become a brigand...{w}{br}But hear me rambling, here take this.
	# Transition out
	t;1
	qr;Woman1;{unit}
	change_tile_sprite;1,11;closed
	set_tile_info;1,11
	replace_tile;1,11;1
	remove_background
	t;2
	# Close the village
	set_origin
	set_tile_info;o0,0
	gold;1000
end