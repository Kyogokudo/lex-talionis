# redundant ?
if;gameStateObj.turncount == 1
    change_ai;Ghreb;HardGuard
end
#lucan mentions the brigands not moving
if;gameStateObj.turncount == 2
	u;Lucan;Left
	s;Lucan;It looks like some of those brigands are still{w}{br} in shock and are considering whether or not to flee.{w}{br} Nevertheless we should warn the townsfolks as soon as we can.
end
# Brigand on the top right side of the screen moves after turn 3
if;gameStateObj.turncount == 4
    change_ai;19,1;PursueVillage
end
# Brigand on the bottom right side of the screen moves after turn 4
if;gameStateObj.turncount == 5
    change_ai;19,14;PursueVillage
end