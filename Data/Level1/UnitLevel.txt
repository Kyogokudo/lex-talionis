# UnitLevel.txt is used to define what units will be part of this level and where they will spawn
# 
# Each unit belongs on its own line
# Syntax:
# New Units:
# team; 0; event_id; class; level; items; position; ai; faction; status (optional)
# - OR -
# Named units:
# team; 1; event_id; unit_id; position; ai
# - OR -
# Created Units:
# team; 2; event_id; class; items; position; ai; faction; status (optional)
# 
# event_id gives the unit a unique id that scripts can use. The unit will not start on the battlefield unless event_id == 0.
# unit_id - unit to load from the units.xml file
# position should be formatted like #,#
# ai refers to what kind of AI the unit should possess.
#
# --------------------------------------------
faction;Rebels;Rebels;Resistance;Civilians opposed to the King's authority.
# Player Characters
player;1;0;Lucan;3,1;None
player;1;0;Colin;2,2;None
player;1;0;Rokh;4,2;None
# Enemies
# Bosses
enemy;0;0;Eldon;17,16;HardGuard
enemy;0;0;Barlow;2,15;DoNothing
# Generics
enemy;0;0;Journeyman;1;Rusted Axe;8,6;HardGuard;Rebels
enemy;0;0;Journeyman;1;Rusted Axe;7,12;HardGuard;Rebels
enemy;0;0;Journeyman;1;Rusted Axe;15,5;HardGuard;Rebels
enemy;0;0;Journeyman;1;Rusted Axe;14,11;HardGuard;Rebels
enemy;0;0;Journeyman;1;Rusted Axe;14,18;HardGuard;Rebels
enemy;0;0;Journeyman;1;Rusted Axe;0,12;HardGuard;Rebels
enemy;0;0;Myrmidon;2;Iron Sword;1,4;SoftGuard;Rebels
enemy;0;0;Soldier;3;Iron Lance;14,16;SoftGuard;Rebels
enemy;0;0;Soldier;4;Steel Lance;14,3;SoftGuard;Rebels
enemy;0;0;Mercenary;3;Iron Sword;6,17;Attack;Rebels
enemy;0;0;Archer;1;Iron Bow;17,9;SoftGuard;Rebels
enemy;0;0;Myrmidon;2;Rusted Sword;11,10;Attack;Rebels
enemy;0;0;Fighter;1;Rusted Axe;6,7;Pursue;Rebels
# === Reinforcements ===
# Player Characters
# Other Characters
other;0;GreenSoldier_1;Soldier;1;;None;None;Rebels
# Enemies
# === Triggers ===
trigger;Watchman;14,16;11,20;14,16
trigger;GroupSep;14,3;14,5;14,3
trigger;GroupSep;6,17;5,16;6,17
trigger;GroupSep;7,12;5,12;7,12
trigger;GroupSep;11,10;5,11;11,10
trigger;Journeyman;14,11;17,10;14,11
trigger;PartySpawn;Rokh;5,-1;4,2
trigger;PartySpawn;Lucan;3,-1;3,1
trigger;PartySpawn;Colin;1,-1;2,2
