﻿victory_screen
#dialogue entre lucan et william
#transition in
t;1
b;Town
t;2
m;Divine Tranquility
u;Lucan;Left
u;William;Right
set_expression;Lucan;HalfCloseEyes
s;Lucan;Even if we had no choice, fighting civilians is just gruesome...{w}{br}How did we come to this ?
set_expression;Lucan;NormalBlink
s;William;It is hard to say. The conditions of life on the countryside are {w}{br}nothing new but for them to be desperate enough to attack us...{w}{br}It feels odd, as if something else was at stake.{w}{br} Still we have to move on.{w}{br} The sooner we subjuguate Falham the better it will be for them.
r;William
#si conditions respectées, arrivée et recrutement de barlow
if;not gameStateObj.check_dead('Barlow')
	if;'JourneymanKilled' not in gameStateObj.level_constants
		m;Stalwarts Unite
		u;Barlow;Right
		s;Barlow;Excuse me. I must thank you for stopping this old fool.
		s;Lucan;You were with the rebels, so why ?
		s;Barlow;You might already be aware of it, but not everyone approves{w}{br} of revolting against the authority. It is true that {w}{br}working the lands is getting increasingly difficult and we have to fund yet another conquest.{w}{br} Still I can't help but think that this invasion isn't for selfish motives.{w}{br} I want to see it for myself, would you be fine with me joining ?
		s;Lucan;This is quite unexpected.{w}{br} I will take up on your offer.{w}{br} If you ever feel like our actions aren't for the benefits{w}{br} of the people I would like you to stop us.
		s;Barlow;I must say I'm surprised to hear this from a knight but I will take you{w}{br} on these words. I am Barlow, I might not have any training but{w}{br} I can still wield an axe pretty fine.
		r;Barlow
		convert;Barlow;player
		set_faction;Barlow;Albion
	else
		kill_unit;Barlow
	end
end