import xml.etree.ElementTree as ET
import numpy as np

def readClass(className):
    tree = ET.parse('../class_info.xml')
    root = tree.getroot()
    clazz = root.find('class[@id="{value}"]'.format(value=className))
    if clazz != None:
        return clazz

def getPromoteLevel():
    with open('../constants.ini', 'r') as f:
        for ln in f:
            if ln.startswith('promoted_level'):
                ln_split = ln.split('=')
                return int(ln_split[1])

def getPromotedGrowthMode():
    with open('../constants.ini', 'r') as f:
        for ln in f:
            if ln.startswith('promoted_growth_mode'):
                ln_split = ln.split('=')
                return int(ln_split[1])
    return 0
    
def calculateStats(clazz, level, skipPromote):
    isPromoted = False
    promoted_level = getPromoteLevel() + 1

    if clazz.find('promotes_from').text and not skipPromote:
        isPromoted = True
        if getPromotedGrowthMode() == 1:
            baseClass = readClass(clazz.find('promotes_from').text)
            bases = calculateStats(baseClass, promoted_level, True)
            promo_gain = clazz.find('promotion').text
            promo_split = promo_gain.split(",")
            promo_gain = np.array([int(i) for i in promo_split])
            bases += promo_gain
        else:
            bases = calculateStats(clazz, promoted_level, True)
    else:
        bases = clazz.find('bases').text
        bases_split = bases.split(",")
        bases = [int(i) for i in bases_split]

    growths = clazz.find('growths').text
    growths_split = growths.split(",")
    growths = [int(i) for i in growths_split]
    
    #1: Fixed - Start each stat at 50. On level-up, add growth rate to stat. When over 100, decrement by 100 and level the stat once
    growth_total = [50] * len(growths)
    gains = [0] * len(bases)
    current_level = 1
    while current_level < level:
        for idx, gr in enumerate(growths):
            growth_total[idx] += gr
            while growth_total[idx] >= 100:
                if growth_total[idx] >= 100:
                    gains[idx] += 1
                    growth_total[idx] -= 100
        current_level += 1
        
    bases = np.array(bases)
    gains = np.array(gains)
    stats = bases + gains
    
    return stats

def getAS(clazz, stats, weaponName):
    if weaponName.startswith('d') or weaponName.startswith('l'):
        weaponName = weaponName[1:]
    tree = ET.parse('../items.xml')
    root = tree.getroot()
    weapon = root.find('item[id="{value}"]'.format(value=weaponName))
    if weapon != None:
        if weapon.find('weight') != None:
            weight = int(weapon.find('weight').text)
            AS = stats[4] - (weight - stats[8]) if weight - stats[8] > 0 else stats[4]
            np.append(stats, AS)
    return stats
    
def getStats(clazz, level, weapon):
    stats = calculateStats(clazz, level, False)
    return getAS(clazz, stats, weapon)
    
def readFile(filename):
    with open(filename, 'r') as f:
        cpt = 0
        for ln in f:
            if ln.startswith('enemy'):
                ln_split = ln.split(';')
                className = ln_split[3]
                clazz = readClass(className)
                if clazz:
                    level = int(ln_split[4])
                    weapon = ln_split[5]
                    weap_split = weapon.split(",")
                    weapon = weap_split[0]
                    stats = getStats(clazz, level, weapon)
                    print(className, level)
                    print(stats)
                    if cpt == 0:
                        total = stats
                    else:
                        total = np.vstack((total, stats))
                    cpt += 1
    means = np.mean(total, axis=0)
    mins = np.min(total, axis=0)
    maxs = np.max(total, axis=0)
    stats_name = ["HP", "Str", "Mag", "Skl", "Spd", "Lck", "Def", "Res", "AS"]
    for idx, name in enumerate(stats_name):
        print("Lowest {0} : {1}".format(name, mins[idx]) )
        print("Highest {0} : {1}".format(name, maxs[idx]) )         
        print("Average {0} : {1} ({2})".format(name, int(np.round(means[idx])), means[idx]))
        print("------------")
        
readFile('UnitLevel.txt')